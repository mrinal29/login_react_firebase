import Login from './Component/Sign-in-up/Login';
import './App.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Sign_up from './Component/Sign-up/Sign_up';
import Dashboard from './Dashboard/Dashboard';


function App() {
  return (
    <Router>
    <div>
      <Switch>
        <Route exact path='/' component = {Login} />
        <Route exact path='/sign_up' component = {Sign_up} />
        <Route exact path='/dashboard' component = {Dashboard} />
        </Switch>
    </div>
    </Router>
  );
}

export default App;
