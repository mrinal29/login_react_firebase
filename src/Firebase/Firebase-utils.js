import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';


const config =  {
    apiKey: "AIzaSyDJCPoReoe1vOWrYKpn81NzcA_TxDqeqdc",
    authDomain: "loginpage-e12b8.firebaseapp.com",
    projectId: "loginpage-e12b8",
    storageBucket: "loginpage-e12b8.appspot.com",
    messagingSenderId: "247597596823",
    appId: "1:247597596823:web:11869e1f9443d6e96469d2",
    measurementId: "G-D447FCELVZ"
  };


  firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);


export default firebase