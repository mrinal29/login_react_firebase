import React, { Component } from 'react'
import CustomButton from '../CustomButton/CustomButton';

import { auth ,signInWithGoogle } from '../../Firebase/Firebase-utils';

import './Sign_in.css'
import Sign_up from '../Sign-up/Sign_up';

export class Sign_in extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
        }

    }
    handleSubmit = async event => {
      event.preventDefault();
  
      const { email, password } = this.state;
  
      try {
        await auth.signInWithEmailAndPassword(email, password);
        this.setState({ email: '', password: '' });
      } catch (error) {
        console.log(error);
      }
    };

    handleChange = event => {
        const { value, name } = event.target;
    
        this.setState({ [name]: value });
      };
    
      render() {
        return (
          <div className='sign-in'>
            <h2>I already have an account</h2>
    
            <form onSubmit={this.handleSubmit}>
            <div>
              <label className='form-label' htmlFor='email'>Email</label>
              <input
                className='form-input'
                name='email'
                type='email'
                handleChange={this.handleChange}
                value={this.state.email}
                autoComplete='off'
                required
              />
            </div>
            <div>
              <label className='form-label' htmlFor='password'>Password</label>
              <input
                className='form-input'
                name='password'
                type='password'
                value={this.state.password}
                handleChange={this.handleChange}
                autoComplete='off'
                required
              />
            </div>
              <CustomButton type='submit'onClick ={()=> {this.props.history.push('/dashboard')}} > Sign in </CustomButton><br/><br/>
              <CustomButton onClick = {signInWithGoogle} >Sign in with Google </CustomButton>
              <br/><br/>
            </form>
            <button className = 'button' onClick ={()=> {this.props.history.push('/sign_up')}}> Sign up </button>
          </div>
        );
      }
    }

export default Sign_in
