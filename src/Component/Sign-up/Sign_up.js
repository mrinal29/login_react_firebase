import React, { Component } from 'react'
import CustomButton from '../CustomButton/CustomButton';
import { auth } from '../../Firebase/Firebase-utils';
import './Sign_up.css'


export class Sign_up extends Component {  
  constructor() {
  super();

  this.state = {
    displayName: '',
    email: '',
    password: '',
    confirmPassword: ''
  };
}

handleSubmit = async event => {
  event.preventDefault();

  const { displayName, email, password, confirmPassword } = this.state;

  if (password !== confirmPassword) {
    alert("passwords don't match");
    return;
  }

  try {
    const { user } = await auth.createUserWithEmailAndPassword(
      email,
      password
    );

    //await createUserProfileDocument(user, { displayName });

    this.setState({
      displayName: '',
      email: '',
      password: '',
      confirmPassword: ''
    });
  } catch (error) {
    console.error(error);
  }
};

handleChange = event => {
  const { name, value } = event.target;

  this.setState({ [name]: value });
};

render() {
  const { displayName, email, password, confirmPassword } = this.state;
  return (
    <div className='sign-up'>
      <h2 className='title'>I do not have a account</h2>
      <span>Sign up with your email and password</span>
      <form className='sign-up-form' onSubmit={this.handleSubmit}>
        <div>
        <label className='form-label' htmlFor="display name">Display Name</label>
        <input
          className='form-input'
          type='text'
          name='displayName'
          value={displayName}
          onChange={this.handleChange}
          label='Display Name'
          required
        />
        </div>
        <div>
        <label className='form-label' htmlFor="email">Email</label>
        <input
          className='form-input'
          type='email'
          name='email'
          value={email}
          onChange={this.handleChange}
          label='Email'
          required
        />
        </div>
        <div>
        <label className='form-label' htmlFor="password">Password</label>
        <input
          className='form-input'
          type='password'
          name='password'
          value={password}
          onChange={this.handleChange}
          required
        />
        </div>
        <div>
        <label className='form-label' htmlFor="confirm password">Confirm Password</label>
        <input
          className='form-input'
          type='password'
          name='confirmPassword'
          value={confirmPassword}
          onChange={this.handleChange}
          required
        />
        </div>
        <CustomButton type='submit'>SIGN UP</CustomButton>
      </form>
    </div>
  );
}
}


export default Sign_up
